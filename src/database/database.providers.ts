import { USERS_DB, BIOMARKERS_DB } from './../constants';
import * as csv from 'csvtojson';

const usersCSV = './src/database/users.csv';
const bioMarkersCSV = './src/database/biomarkers.csv';

export const databaseProviders = [
  {
    provide: USERS_DB,
    useFactory: async () => {
      const usersArray = await csv().fromFile(usersCSV);
      return usersArray;
    },
  },
  {
    provide: BIOMARKERS_DB,
    useFactory: async () => {
      const bioMarkersArray = await csv().fromFile(bioMarkersCSV);
      return bioMarkersArray;
    },
  },
];
