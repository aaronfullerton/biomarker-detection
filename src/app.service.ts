import { Injectable, Inject } from '@nestjs/common';
import { USERS_DB, BIOMARKERS_DB } from './constants';
import { propSatisfies, filter, propEq, both, find, cond, always } from 'ramda';
import * as moment from 'moment';

@Injectable()
export class AppService {
  constructor(
    @Inject(USERS_DB) private readonly usersDb,
    @Inject(BIOMARKERS_DB) private readonly biomarkersDb,
  ) {}

  getUsers() {
    return this.usersDb;
  }

  getBiomarkers() {
    return this.biomarkersDb.map(biomarker => {
      return Object.assign({}, biomarker, {
        value: parseFloat(biomarker.value),
        occurred_on: moment(new Date(biomarker.occurred_on)),
      });
    });
  }

  getLast7DaysOfGlucoseForBiomarker(biomarker) {
    const sevenDaysAgo = biomarker.occurred_on.subtract(7, 'days');

    const last7Days = filter(
      both(
        propSatisfies(value => value.isAfter(sevenDaysAgo), 'occurred_on'),
        propEq('biomarker_type', 'glucose'),
      ),
    );

    return last7Days(this.getBiomarkers());
  }

  getBiomarkerById(id: number) {
    const findBiomarker = find(propEq('id', id));
    return findBiomarker(this.getBiomarkers());
  }

  checkForAlerts(biomarker) {
    const hasHighGlucose = both(
      propEq('biomarker_type', 'glucose'),
      propSatisfies(value => value > 140, 'value'),
    );

    const hasHighKetones = both(
      propEq('biomarker_type', 'ketone'),
      propSatisfies(value => value > 7.0, 'value'),
    );

    const hasRepeatedHighGlucose = () => {
      const glucoses = this.getLast7DaysOfGlucoseForBiomarker(biomarker);
      let count = 0;

      glucoses.forEach(glucoseBiomarker => {
        if (hasHighGlucose(glucoseBiomarker)) {
          count++;
        }
      });

      return count >= 3;
    };

    return cond([
      [hasRepeatedHighGlucose, always('Repeatedly high glucose')],
      [hasHighGlucose, always('Glucose is high')],
      [hasHighKetones, always('Ketones are high')],
    ])(biomarker);
  }
}
