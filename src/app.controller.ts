import { Get, Controller, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('/users')
  getUsers() {
    return this.appService.getUsers();
  }

  @Get('/biomarkers')
  getBiomarkers() {
    return this.appService.getBiomarkers();
  }

  @Get('/biomarkers/:id/alerts')
  getBiomarkerAlerts(@Param('id') id) {
    return this.appService.checkForAlerts(this.appService.getBiomarkerById(id));
  }
}
